package nz.ac.massey.driver;


import nz.ac.massey.driver.generator.AppClassVisitor;
import static org.junit.Assert.*;

import nz.ac.massey.driver.generator.DriverGenerator;
import nz.ac.massey.driver.model.JUnit4Model;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.ClassReader;

/**
 * Test class visitor
 */

public class JUnit4Testcase {

    private JUnit4Model junit4;

    @Before
    public void setup()throws Exception{
        junit4=new JUnit4Model();
    }

    @After
    public void clean(){junit4=null;}

    @Test
    public void testClassVisitor() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);

        //check for setup methods
        assertEquals("beforeClass",junit4.getBeforeClass().getName());
        assertEquals("setup",junit4.getBefore().getName());
        assertEquals("clean",junit4.getAfter().getName());
        assertEquals("afterClass",junit4.getAfterClass().getName());
        //check for tests
        assertEquals(3,junit4.getTests().size());
        assertEquals("test1",junit4.getTests().get(0).getName());
        assertEquals("test2",junit4.getTests().get(1).getName());
        assertEquals("test_exception",junit4.getTests().get(2).getName());
        //check for class name
        assertEquals("nz.ac.massey.driver.cases.ExampleJUnit4",junit4.getClassName());
    }
    @Test
    public void testGeneratedTemplate() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        String expected="//tests for class:nz.ac.massey.driver.cases.ExampleJUnit4\n" +
                "public class Driver{\n" +
                "public static void main(String[] args){\n" +
                "\ttry{\n" +
                "nz.ac.massey.driver.cases.ExampleJUnit4 test0=new nz.ac.massey.driver.cases.ExampleJUnit4();\n" +
                "         test0.beforeClass();\n" +
                "         test0.setup();\n" +
                "         test0.test1();\n" +
                "         test0.clean();\n" +
                "         test0.afterClass();\n" +
                "\t}catch(Throwable e){ System.err.println(e.getMessage());}\n" +
                "\ttry{\n" +
                "nz.ac.massey.driver.cases.ExampleJUnit4 test1=new nz.ac.massey.driver.cases.ExampleJUnit4();\n" +
                "         test1.beforeClass();\n" +
                "         test1.setup();\n" +
                "         test1.test2();\n" +
                "         test1.clean();\n" +
                "         test1.afterClass();\n" +
                "\t}catch(Throwable e){ System.err.println(e.getMessage());}\n" +
                "\ttry{\n" +
                "nz.ac.massey.driver.cases.ExampleJUnit4 test2=new nz.ac.massey.driver.cases.ExampleJUnit4();\n" +
                "         test2.beforeClass();\n" +
                "         test2.setup();\n" +
                "    try{\n" +
                "         test2.test_exception();\n" +
                "    }catch(java.lang.IndexOutOfBoundsException e){}\n" +
                "         test2.clean();\n" +
                "         test2.afterClass();\n" +
                "\t}catch(Throwable e){ System.err.println(e.getMessage());}\n" +
                "}}";
;
        assertEquals(expected,junit4.buildTestTemplate("Driver").toString());
    }

    @Test
    public void testSuperClassSetup() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4SubClass");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);

        assertEquals("nz.ac.massey.driver.cases.ExampleJUnit4SuperClass",junit4.getSuperclass().getClassName());

        String expected="//tests for class:nz.ac.massey.driver.cases.ExampleJUnit4SubClass\n" +
                "public class Driver{\n" +
                "public static void main(String[] args){\n" +
                "\ttry{\n" +
                "nz.ac.massey.driver.cases.ExampleJUnit4SubClass test0=new nz.ac.massey.driver.cases.ExampleJUnit4SubClass();\n" +
                "         test0.setup();\n" +
                "         test0.test1();\n" +
                "         test0.tearDown();\n" +
                "\t}catch(Throwable e){ System.err.println(e.getMessage());}\n" +
                "\ttry{\n" +
                "nz.ac.massey.driver.cases.ExampleJUnit4SubClass test1=new nz.ac.massey.driver.cases.ExampleJUnit4SubClass();\n" +
                "         test1.setup();\n" +
                "         test1.test2();\n" +
                "         test1.tearDown();\n" +
                "\t}catch(Throwable e){ System.err.println(e.getMessage());}\n" +
                "}}";

        assertEquals(expected,junit4.buildTestTemplate("Driver").toString());
    }

    @Test
    public void testInitStatement() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);

        String expected="nz.ac.massey.driver.cases.ExampleJUnit4 test=new nz.ac.massey.driver.cases.ExampleJUnit4();\n";
        assertEquals(expected,junit4.getConstructor().buildInitStatement("test"));
    }

    @Test
    public void testNumberOfArguments() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4");
        AppClassVisitor cv1 = new AppClassVisitor(junit4);
        reader.accept(cv1, ClassReader.SKIP_CODE);

        assertEquals(Integer.valueOf(0),junit4.getConstructor().getNumberOfArgsInConstructor());
    }


    @Test
    public void testBuildTryCatch() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);

        String expected=
                "    try{\n" +
                "         test.test_exception();\n" +
                "    }catch(java.lang.IndexOutOfBoundsException e){}\n";

        assertEquals(expected,junit4.getTests().get(2).buildMethod("test"));
    }


    @Test
    public void testParameterizedTestViaFieldInjection() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaFieldInjection");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        //contain only one test
        assertEquals(1,junit4.getTests().size());
        assertEquals("test_parameter",junit4.getTests().get(0).getName());
        //fields
        assertEquals("number",junit4.getField().get(0).getName());
        assertEquals("expected_number",junit4.getField().get(1).getName());
        //no arguments being used for parameterized test
        assertEquals(Integer.valueOf(0),junit4.getConstructor().getNumberOfArgsInConstructor());
        String expected="nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaFieldInjection test=new nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaFieldInjection();\n";
        assertEquals(expected,junit4.getConstructor().buildInitStatement("test"));
        //the one use to pass parameter method is called "myParameters"
        assertEquals("myParameters",junit4.getParameterMethod().getName());
    }


    @Test
    public void testParameterizedTestViaConstructor() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaConstructor");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        //contain only one test
        assertEquals(1,junit4.getTests().size());
        assertEquals("test_parameter",junit4.getTests().get(0).getName());
        //no field being used
        assertEquals(0,junit4.getField().size());
        //two arguments being used for parameterized test
        assertEquals(Integer.valueOf(2),junit4.getConstructor().getNumberOfArgsInConstructor());
        String expected="nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaConstructor test=new nz.ac.massey.driver.cases.ExampleJUnit4ParameterViaConstructor((Integer)this_is_parameter[0],(Integer)this_is_parameter[1]);\n";
        assertEquals(expected,junit4.getConstructor().buildInitStatement("test"));
        //the one use to pass parameter method is called "myParameters"
        assertEquals("myParameters",junit4.getParameterMethod().getName());
    }


    @Test
    public void testDifferentFieldTypes() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4FieldTypes");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        //number of field being tested
        assertEquals(11,junit4.getField().size());
        //test each field been generated
        assertEquals("int i",junit4.getField().get(0).buildField());
        assertEquals("float f",junit4.getField().get(1).buildField());
        assertEquals("short s",junit4.getField().get(2).buildField());
        assertEquals("boolean z",junit4.getField().get(3).buildField());
        assertEquals("char c",junit4.getField().get(4).buildField());
        assertEquals("byte b",junit4.getField().get(5).buildField());
        assertEquals("double d",junit4.getField().get(6).buildField());
        assertEquals("long l",junit4.getField().get(7).buildField());
        assertEquals("Object string",junit4.getField().get(8).buildField());
        assertEquals("Object list",junit4.getField().get(9).buildField());
        assertEquals("Object integer",junit4.getField().get(10).buildField());
    }

    @Test
    public void testDifferentFieldBoxing()throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4FieldTypes");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        //number of field being tested
        assertEquals(11,junit4.getField().size());
        //test each field been generated
        assertEquals("(Integer)",junit4.getField().get(0).buildFieldBoxing());
        assertEquals("(Float)",junit4.getField().get(1).buildFieldBoxing());
        assertEquals("(Short)",junit4.getField().get(2).buildFieldBoxing());
        assertEquals("(Boolean)",junit4.getField().get(3).buildFieldBoxing());
        assertEquals("(Character)",junit4.getField().get(4).buildFieldBoxing());
        assertEquals("(Byte)",junit4.getField().get(5).buildFieldBoxing());
        assertEquals("(Double)",junit4.getField().get(6).buildFieldBoxing());
        assertEquals("(Long)",junit4.getField().get(7).buildFieldBoxing());
        assertEquals("(Object)",junit4.getField().get(8).buildFieldBoxing());
        assertEquals("(Object)",junit4.getField().get(9).buildFieldBoxing());
        assertEquals("(Object)",junit4.getField().get(10).buildFieldBoxing());
    }

    @Test
    public void testDifferentArgsBoxing() throws Exception{
        ClassReader reader = new ClassReader("nz.ac.massey.driver.cases.ExampleJUnit4FieldTypes");
        AppClassVisitor cv = new AppClassVisitor(junit4);
        reader.accept(cv, ClassReader.SKIP_CODE);
        //number of args
        assertEquals(Integer.valueOf(11),junit4.getConstructor().getNumberOfArgsInConstructor());
        //expected class init with multiple args
        String expected="nz.ac.massey.driver.cases.ExampleJUnit4FieldTypes test=new nz.ac.massey.driver.cases.ExampleJUnit4FieldTypes((Integer)this_is_parameter[0],(Float)this_is_parameter[1],(Short)this_is_parameter[2],(Boolean)this_is_parameter[3],(Character)this_is_parameter[4],(Byte)this_is_parameter[5],(Double)this_is_parameter[6],(Long)this_is_parameter[7],(Object)this_is_parameter[8],(Object)this_is_parameter[9],(Object)this_is_parameter[10]);\n";
        assertEquals(expected,junit4.getConstructor().buildInitStatement("test"));
    }
}
