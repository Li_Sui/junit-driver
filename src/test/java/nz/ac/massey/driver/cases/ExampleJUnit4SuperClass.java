package nz.ac.massey.driver.cases;

import org.junit.After;

import org.junit.Before;

public class ExampleJUnit4SuperClass {

    @Before
    public void setup(){
        System.out.println("setup");
    }

    @After
    public void tearDown(){
        System.out.println("tearDown");
    }
}
