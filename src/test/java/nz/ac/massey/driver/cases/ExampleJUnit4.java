package nz.ac.massey.driver.cases;

import org.junit.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is used for test
 */

public class ExampleJUnit4 {

    @BeforeClass
    public static void beforeClass(){ System.out.println("BeforeClass");}
    @Before
    public void setup(){
        System.out.println("BeforeTest");
    }
    @After
    public void clean(){
        System.out.println("AfterTest");
    }

    @Test
    public void test1(){
        System.out.println("Doing test1");
    }
    @Test
    public void test2(){
        System.out.println("Doing test2");
    }

    @AfterClass
    public static void afterClass(){System.out.println("AfterClass");}

    @Test(expected = IndexOutOfBoundsException.class)
    public void test_exception() {
        new ArrayList<Object>().get(0);
    }
}