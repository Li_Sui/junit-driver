package nz.ac.massey.driver.cases;

import org.junit.Test;

public class ExampleJUnit4SubClass extends ExampleJUnit4SuperClass{
    @Test
    public void test1(){
        System.out.println("test1");
    }

    @Test
    public void test2(){
        System.out.println("test2");
    }
}
