package nz.ac.massey.driver.cases;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

/**
 * This class is used for test
 */
@RunWith(value= Parameterized.class)
public class ExampleJUnit4FieldTypes {

    @Parameterized.Parameter(value = 0)//indicate the position of parameter
    public int i;
    @Parameterized.Parameter(value = 1)//indicate the position of parameter
    public float f;
    @Parameterized.Parameter(value = 2)//indicate the position of parameter
    public short s;
    @Parameterized.Parameter(value = 3)//indicate the position of parameter
    public boolean z;
    @Parameterized.Parameter(value = 4)//indicate the position of parameter
    public char c;
    @Parameterized.Parameter(value = 5)//indicate the position of parameter
    public byte b;
    @Parameterized.Parameter(value = 6)//indicate the position of parameter
    public double d;
    @Parameterized.Parameter(value = 7)//indicate the position of parameter
    public long l;
    @Parameterized.Parameter(value = 8)//indicate the position of parameter
    public String string;
    @Parameterized.Parameter(value = 9)//indicate the position of parameter
    public List<Integer> list;
    @Parameterized.Parameter(value = 10)//indicate the position of parameter
    public Integer integer;


    public ExampleJUnit4FieldTypes(int i,float f,short s,boolean z,char c,byte b,double d,long l,String string, List<Integer> list, Integer integer){

    }
}