package nz.ac.massey.driver.cases;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is used for test
 */
@RunWith(value= Parameterized.class)
public class ExampleJUnit4ParameterViaFieldInjection {

    @Parameterized.Parameter(value = 0)//indicate the position of parameter
    public int number;
    @Parameterized.Parameter(value = 1)
    public int expected_number;

    @Parameterized.Parameters
    public static List<Integer[]> myParameters(){
        return Arrays.asList(new Integer[][]{{1,1},{2,2},{3,3}});
    }

    @Test
    public void test_parameter(){
        Assert.assertEquals(expected_number,number);
    }

}