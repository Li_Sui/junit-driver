package nz.ac.massey.driver;

import nz.ac.massey.driver.generator.DriverGenerator;

/**
 * Created by li on 7/06/17.
 */
public class Main{

    public static void main(final String[] args) throws Exception{

        if (args.length < 1)
        {
            System.err.println("please specify input:");
            System.err.println("requried: args[0]=junit4||junit3,args[1]=inputDir,args[2]=outputDir");
            return;
        }

        String defaultModel="junit4";

        if(args[0].equals("junit4")){
            System.out.println("running with junit4, processing dir:" + args[1]);
            System.out.println("out dir: " + args[2]);
            new DriverGenerator("junit4", args[1], args[2]).generate();
        }
        if(args[0].equals("junit3")){
            System.out.println("running with junit3, processing dir:" + args[1]);
            System.out.println("out dir: " + args[2]);
            new DriverGenerator("junit3", args[1], args[2]).generate();
        }

        if(args[0].equals("evosuite")){
            System.out.println("running with evosuite tests, processing dir:" + args[1]);
            System.out.println("out dir: " + args[2]);
            new DriverGenerator("evosuite", args[1], args[2]).generateForEvosuite();

        }

    }
}
