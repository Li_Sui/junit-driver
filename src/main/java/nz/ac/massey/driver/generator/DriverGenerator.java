package nz.ac.massey.driver.generator;

import nz.ac.massey.driver.model.AbstractModel;
import nz.ac.massey.driver.model.EvoSuiteModel;
import nz.ac.massey.driver.model.JUnit3Model;
import nz.ac.massey.driver.model.JUnit4Model;
import nz.ac.massey.driver.structure.DriverClass;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassReader;

import java.io.*;

import java.util.*;


/**
 *
 * input : jar file
 * output: driver in source code
 * Created by Li Sui
 */
public class DriverGenerator {

    private String inputDir;
    private String outputDir;
    private String whichModel;

    public DriverGenerator(String whichModel, String inputDir,String outputDir){
        this.whichModel=whichModel;
        this.inputDir=inputDir;
        this.outputDir=outputDir;
    }

    public void generate() throws Exception{
        Iterator it = FileUtils.iterateFiles(new File(inputDir),null,true);
        StringBuilder mainClass=new StringBuilder("public class EntryPoint{\n" +
        "   public static void main(String[] args) throws Throwable{\n");
        int counter=0;
        while(it.hasNext()){
            File f = (File) it.next();
            if(f.getName().endsWith(".class")){
                 InputStream stream =new FileInputStream(f);
                AbstractModel model=visitClasses(stream);
                if(model.getTests().size()!=0 && model.getClassName()!=null){

                    //write driver to file
                    writeToFile(model.buildTestTemplate("Driver"+counter).toString(),"Driver"+counter+".java");
                    mainClass.append("\tDriver"+counter + ".main(args);\n");
                    counter++;
                }
            }
        }

        mainClass.append("  }\n"+ "}");
        //write entryPoint to file
        writeToFile(mainClass.toString(),"EntryPoint.java");
    }

    public void generateForEvosuite()throws Exception{
        Iterator it = FileUtils.iterateFiles(new File(inputDir),null,true);
        int counter=0;
        StringBuilder mainClass=new StringBuilder("public class Driver{\n" +
                "   public static void main(String[] args) throws Throwable{\n");
        while(it.hasNext()){
            File f = (File) it.next();
            if(f.getName().endsWith(".class")){
                InputStream stream =new FileInputStream(f);
                AbstractModel model=visitClasses(stream);
                if(model.getTests().size()!=0 && model.getClassName()!=null){
                    mainClass.append(model.buildTestTemplate("driver"+counter));

                    counter++;
                }
            }
        }
        mainClass.append("  }\n"+ "}");
        writeToFile(mainClass.toString(),"Driver.java");
    }


    public AbstractModel visitClasses(InputStream stream) throws Exception{
        AbstractModel model=null;
        if(whichModel.equals("junit4")) {
           model=new JUnit4Model();
        }
        if(whichModel.equals("junit3")){
            model=new JUnit3Model();
        }
        if(whichModel.equals("evosuite")){
            model=new EvoSuiteModel();
        }
        ClassReader reader = new ClassReader(stream);
        AppClassVisitor cv = new AppClassVisitor(model);
        reader.accept(cv, 0);
        stream.close();
        return model;
    }


    private void writeToFile(String results, String fileName) throws Exception{
        File file = new File(outputDir+"/"+fileName);
        file.getParentFile().mkdirs();
        BufferedWriter writer=new BufferedWriter( new FileWriter( file));
        writer.write(results);
        writer.close();
    }

}
