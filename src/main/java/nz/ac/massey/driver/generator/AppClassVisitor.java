package nz.ac.massey.driver.generator;

import nz.ac.massey.driver.model.AbstractModel;
import nz.ac.massey.driver.model.EvoSuiteModel;
import nz.ac.massey.driver.model.JUnit3Model;
import nz.ac.massey.driver.model.JUnit4Model;
import nz.ac.massey.driver.structure.JUnitConstructor;
import nz.ac.massey.driver.structure.JUnitField;
import nz.ac.massey.driver.structure.JUnitMethod;
import org.objectweb.asm.*;



/**
 * This class is used to read the structure of JUnit class
 * Created by li on 26/05/17.
 */
public class AppClassVisitor extends ClassVisitor {

    public String className;


    public AbstractModel model=null;

    public AppClassVisitor(AbstractModel model) {
        super(Opcodes.ASM5);
        this.model=model;
    }


    public AbstractModel getModel(){
        return this.model;
    }
    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        className = name.replace("/",".");

        if(access != 1057 ){// not an abstract class
            model.setClassName(this.className);
            if(superName!=null && !(model instanceof EvoSuiteModel)) { //if super class exist, explore the super class DISABLE IT FOR XCORPUS PROJECT
                String superClassName = superName.replace("/", ".");

                try {
                    AbstractModel superClassModel = null;
                    if (model instanceof JUnit3Model) {
                        superClassModel = new JUnit3Model();
                    }
                    if (model instanceof JUnit4Model) {
                        superClassModel = new JUnit4Model();
                    }
                    ClassReader reader = new ClassReader(superClassName);
                    AppClassVisitor cv = new AppClassVisitor(superClassModel);
                    reader.accept(cv, ClassReader.SKIP_CODE);
                    model.setSuperclass(superClassModel);

                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        }
    }
    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
//        if(name.contains("test")) {
//            model.checkForTests("junitTest", name);
//        }
        if(name.equals("<init>")) {
            model.setConstructor(new JUnitConstructor(Type.getArgumentTypes(desc),className));
        }
        return new AppMethodVisitor(name,desc);
    }

    @Override
    public FieldVisitor visitField(int i, String name, String type, String s2, Object o) {
        return new AppFiledVisitor(name,type);
    }

    @Override
    public AnnotationVisitor visitAnnotation(String desc, boolean b) {
        return new AppAnnotationVisitor();
    }

    public class AppFiledVisitor extends FieldVisitor{
        private String name;
        private String type;
        public AppFiledVisitor(String name,String type){
            super(Opcodes.ASM5);
            this.name=name;
            this.type=type;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String s, boolean b) {
            return new AppAnnotationVisitor(name,type);
        }
    }

    public class AppMethodVisitor extends MethodVisitor {

        private String name;
        private String returnType;

        public AppMethodVisitor(String name,String returnType) {
            super(Opcodes.ASM5);
            this.name=name;
            this.returnType=returnType;
        }


        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            JUnitMethod m=new JUnitMethod(name,returnType);
            model.checkForTests(desc,m);
            return new AppAnnotationVisitor(m);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
//            if(owner.equals("junit/framework/TestCase")){
//                model.checkForTests("",name);
//            }
            super.visitMethodInsn(opcode, owner, name, desc, itf);
        }
    }


    public class AppAnnotationVisitor extends AnnotationVisitor{
        private String fieldName;
        private String fieldType;
        private JUnitMethod method;

        public AppAnnotationVisitor(String fieldName, String fieldType){
            super(Opcodes.ASM5);
            this.fieldName=fieldName;
            this.fieldType=fieldType;
        }

        public AppAnnotationVisitor(JUnitMethod method){
            super(Opcodes.ASM5);
            this.method=method;
        }

        public AppAnnotationVisitor() {
            super(Opcodes.ASM5);
        }

        @Override
        public void visit(String variable, Object value) {

            //encounter annotation with exception handling
            if(variable.equals("expected")){
                String v=value.toString().substring(1,value.toString().length()-1);
                method.setException(v.replace("/","."));
            }
            //encounter field annotation that runs with parameterized tests
            if(variable.equals("value") && value instanceof Integer){
                model.addField(new JUnitField(Integer.valueOf(value.toString()),fieldName,fieldType));//desc in this case refers to field type
            }
            //encounter annotation that runs with parameterized tests
            if(variable.equals("value") && value.toString().equals("Lorg/junit/runners/Parameterized;")){
               model.setToParameterizedTest(true);
            }

            super.visit(variable, value);
        }
    }
}
