package nz.ac.massey.driver.structure;

public class DriverClass {
    private String name;
    private String template;

    public DriverClass(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public String getTemplate(){return template;}

    public void setTemplate(String template){this.template=template;}
}
