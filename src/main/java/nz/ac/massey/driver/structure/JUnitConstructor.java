package nz.ac.massey.driver.structure;

import org.objectweb.asm.Type;

public class JUnitConstructor {

    private Integer numberOfArgsInConstructor;
    private Type[] argumentsType;
    private String className;

    public JUnitConstructor(Type[] argumentsType, String className) {
        this.argumentsType=argumentsType;
        this.numberOfArgsInConstructor = argumentsType.length;
        this.className = className;
    }



    public Integer getNumberOfArgsInConstructor() {
        return numberOfArgsInConstructor;
    }



    public String buildInitStatement(String instanceName){
        StringBuilder initStatement=new StringBuilder();
        StringBuilder arguments=new StringBuilder("");
        for(int i = 0; i< getNumberOfArgsInConstructor(); i++){
            if(i< getNumberOfArgsInConstructor()-1){
                arguments.append(buildParameterBoxing(i)+"this_is_parameter["+i+"],");
            }else{
                arguments.append(buildParameterBoxing(i)+"this_is_parameter["+i+"]");
            }
        }
        initStatement.append(this.className + " " + instanceName + "=new " + this.className + "("+arguments+");\n");
        return initStatement.toString();
    }

    private String buildParameterBoxing(int argPosition){
        String cast="(Object)";
        if(argumentsType[argPosition].toString().equals("I")){
            cast="(Integer)";
        }else
        if(argumentsType[argPosition].toString().equals("D")){
            cast= "(Double)";
        }else
        if(argumentsType[argPosition].toString().equals("F")){
            cast= "(Float)";
        }else
        if(argumentsType[argPosition].toString().equals("B")){
            cast= "(Byte)";
        }else
        if(argumentsType[argPosition].toString().equals("C")){
            cast= "(Character)";
        }else
        if(argumentsType[argPosition].toString().equals("J")){
            cast= "(Long)";
        }else
        if(argumentsType[argPosition].toString().equals("S")){
            cast= "(Short)";
        }else
        if(argumentsType[argPosition].toString().equals("Z")){
            cast= "(Boolean)";
        }
        return cast;
    }
}
