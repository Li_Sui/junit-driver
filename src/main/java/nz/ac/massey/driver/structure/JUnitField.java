package nz.ac.massey.driver.structure;

public class JUnitField {

    private Integer order;
    private String name;
    private String type;

    public JUnitField(Integer order, String name, String type){
        this.order=order;
        this.name=name;
        this.type=type;
    }

    public Integer getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String buildField(){

        if(type.equals("I")){
            return "int "+name;
        }else
        if(type.equals("D")){
            return "double "+name;
        }else
        if(type.equals("F")){
            return "float "+name;
        }else
        if(type.equals("B")){
            return "byte "+name;
        }else
        if(type.equals("C")){
            return "char "+name;
        }else
        if(type.equals("J")){
            return "long "+name;
        }else
        if(type.equals("S")){
            return "short "+name;
        }else
        if(type.equals("Z")){
            return "boolean "+name;
        }else{
            return "Object "+name;
        }
    }

    public String buildFieldBoxing(){
        String cast="(Object)";
        if(type.equals("I")){
            cast="(Integer)";
        }else
        if(type.equals("D")){
            cast= "(Double)";
        }else
        if(type.equals("F")){
            cast= "(Float)";
        }else
        if(type.equals("B")){
            cast= "(Byte)";
        }else
        if(type.equals("C")){
            cast= "(Character)";
        }else
        if(type.equals("J")){
            cast= "(Long)";
        }else
        if(type.equals("S")){
            cast= "(Short)";
        }else
        if(type.equals("Z")){
            cast= "(Boolean)";
        }
        return cast;
    }
}
