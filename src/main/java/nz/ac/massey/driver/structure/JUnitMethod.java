package nz.ac.massey.driver.structure;

public class JUnitMethod {

    private String name;

    private String exception;

    private String returnType;

    public JUnitMethod(String name, String returnType) {
        this.name=name;
        this.returnType=returnType;
    }

    public String getName() {
        return name;
    }

    public String getException() {
        return exception;
    }

    public String getReturnType(){return returnType;}

    public void setException(String exception) {
        this.exception = exception;
    }

    public String buildMethod(String instanceName){
        if(exception!=null){
            StringBuilder tryCatch =new StringBuilder();
            tryCatch.append("    try{\n");
            tryCatch.append("         " + instanceName + "." + name + "();\n");
            tryCatch.append("    }catch(");
            tryCatch.append(exception+" e");
            tryCatch.append("){}\n");//TODO:catch the correct exception

            return tryCatch.toString();
        }else {
            return "         " + instanceName + "." + name + "();\n";
        }
    }
}
