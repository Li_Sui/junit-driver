package nz.ac.massey.driver.model;

import nz.ac.massey.driver.structure.DriverClass;
import nz.ac.massey.driver.structure.JUnitMethod;

import java.util.List;

/**
 * Created by li on 30/05/17. NOT FULLY IMPLEMENTED
 */
public class JUnit3Model extends AbstractModel {

    private JUnitMethod before;
    private JUnitMethod after;

    private void setBefore(JUnitMethod before) {
        this.before = before;
    }

    private void setAfter(JUnitMethod after) {
        this.after = after;
    }

    public JUnitMethod getBefore() {
        return before;
    }

    public JUnitMethod getAfter() {
        return after;
    }

    public JUnit3Model(){
        super();
    }

    public StringBuilder buildInit(String testName,String arguments) {
        //TODO:
        return null;
    }

    public StringBuilder buildTestTemplate(String classInstanceName) {
        StringBuilder test =new StringBuilder();
        String testName="test";
        test.append(this.getClassName()+" "+testName+"=new "+this.getClassName()+"(); \n");
        if(getTests()!=null) {
            for(JUnitMethod m: getTests()) {
                //@Before
                if(this.before!=null) {
                    test.append("         "+testName+"." + this.before+"();\n");
                }
                //@Test
                test.append("         "+testName+"." + m + "();\n");
                //@After
                if(this.after!=null) {
                    test.append("         "+testName+"." + this.after+"();\n");
                }
            }
        }
        test.append("\n\n");

        return test;
    }

    public void checkForTests(String desc, JUnitMethod name){
        if(name.equals("setUp")){
            setBefore(name);
        }
        if(name.equals("tearDown")){
            setAfter(name);
        }
        if(desc.equals("junitTest")){
            addTest(name);
        }
    }

}
