package nz.ac.massey.driver.model;

import nz.ac.massey.driver.structure.JUnitConstructor;
import nz.ac.massey.driver.structure.JUnitField;
import nz.ac.massey.driver.structure.JUnitMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by li on 30/05/17.
 */
public abstract class AbstractModel implements ModelInterface {

    private boolean isParameterizedTest=false;

    private AbstractModel superclass;

    private String className;

    private List<JUnitMethod> tests;

    private JUnitConstructor constructor;

    private List<JUnitField> field;

    public AbstractModel(){

        tests=new ArrayList<JUnitMethod>();
        field=new ArrayList<JUnitField>();

    }

    public void setConstructor(JUnitConstructor constructor) {
        this.constructor = constructor;
    }
    public JUnitConstructor getConstructor(){return this.constructor;}

    public void addField(JUnitField field){this.field.add(field);}
    public List<JUnitField> getField(){return this.field;}


    public void setToParameterizedTest(boolean isParameterizedTest){this.isParameterizedTest=isParameterizedTest;}
    public boolean isParameterizedTest(){return this.isParameterizedTest;}


    public void addTest(JUnitMethod test) {
        this.tests.add(test);
    }
    public List<JUnitMethod> getTests() {
        return tests;
    }

    public String getClassName() {
        return className;
    }
    public void setClassName(String className){
        this.className=className;
    }

    public AbstractModel getSuperclass(){return superclass;}
    public void setSuperclass(AbstractModel superclass){this.superclass=superclass;}
}
