package nz.ac.massey.driver.model;

import nz.ac.massey.driver.generator.AppClassVisitor;
import nz.ac.massey.driver.structure.JUnitMethod;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassReader;

import java.io.*;
import java.util.Iterator;

public class EvoSuiteModel extends AbstractModel {
    @Override
    public StringBuilder buildTestTemplate(String classInstanceName) {
        StringBuilder template =new StringBuilder();

        template.append("\t//tests for class:"+getClassName()+"\n");//append comments

        template.append(getConstructor().buildInitStatement(classInstanceName));
        if(getTests()!=null) {
            for(int i=0;i<getTests().size();i++) {
                template.append("\ttry{\n");
                template.append(getTests().get(i).buildMethod(classInstanceName));
                template.append("\t}catch(Throwable e){ System.err.println(e.getMessage());}\n");//end of main method
            }
        }
        return  template;
    }

    @Override
    public void checkForTests(String desc, JUnitMethod method) {
        if(desc.equals("Lorg/junit/Test;")){
            addTest(method);
        }
    }

}
