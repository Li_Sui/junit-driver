package nz.ac.massey.driver.model;

import nz.ac.massey.driver.generator.DriverGenerator;
import nz.ac.massey.driver.structure.JUnitField;
import nz.ac.massey.driver.structure.JUnitMethod;
import nz.ac.massey.driver.structure.DriverClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by li on 30/05/17.
 */
public class JUnit4Model extends AbstractModel {

    private JUnitMethod beforeClass;
    private JUnitMethod afterClass;
    private JUnitMethod before;
    private JUnitMethod after;
    private JUnitMethod parameterMethod;

    public JUnit4Model(){
        super();
    }

    private void setBeforeClass(JUnitMethod beforeClass) {
        this.beforeClass = beforeClass;
    }

    private void setAfterClass(JUnitMethod afterClass) {
        this.afterClass = afterClass;
    }

    private void setBefore(JUnitMethod before) {
        this.before = before;
    }

    private void setAfter(JUnitMethod after) {
        this.after = after;
    }

    public JUnitMethod getBeforeClass() {
        return beforeClass;
    }

    public JUnitMethod getAfterClass() {
        return afterClass;
    }

    public JUnitMethod getBefore() {
        return before;
    }

    public JUnitMethod getAfter() {
        return after;
    }

    public JUnitMethod getParameterMethod(){return this.parameterMethod;}

    public void setParameterMethod(JUnitMethod parameterMethod){this.parameterMethod=parameterMethod;}




    public StringBuilder buildTest(String instanceName, JUnitMethod testMethod){
        StringBuilder test =new StringBuilder();

        //this is the case that parameters are injected via fields
        if (getField().size() != 0 && getConstructor().getNumberOfArgsInConstructor() == 0) {
            //add fields
            for (JUnitField f : getField()) {
                test.append("           " + f.getName() + "=" + f.buildFieldBoxing() + "this_is_parameter[" + f.getOrder() + "];\n");
            }
        }
        test.append(getConstructor().buildInitStatement(instanceName));

        if(getSuperclass()!=null){
            if(((JUnit4Model)getSuperclass()).beforeClass!=null) {
                test.append(((JUnit4Model)getSuperclass()).getBeforeClass().buildMethod(instanceName));
            }
        }

        //@BeforeClass
        if(this.beforeClass!=null) {
            test.append(this.beforeClass.buildMethod(instanceName));
        }

        if(getSuperclass()!=null){
            if(((JUnit4Model)getSuperclass()).before!=null) {
                test.append(((JUnit4Model)getSuperclass()).getBefore().buildMethod(instanceName));
            }
        }
        //@Before
        if(this.before!=null) {
            test.append(this.before.buildMethod(instanceName));
        }
        //@Test
        test.append(testMethod.buildMethod(instanceName));

        if(getSuperclass()!=null){
            if(((JUnit4Model)getSuperclass()).after!=null) {
                test.append(((JUnit4Model)getSuperclass()).getAfter().buildMethod(instanceName));
            }
        }
        //@After
        if(this.after!=null) {
            test.append(this.after.buildMethod(instanceName));
        }

        if(getSuperclass()!=null){
            if(((JUnit4Model)getSuperclass()).afterClass!=null) {
                test.append(((JUnit4Model)getSuperclass()).getAfterClass().buildMethod(instanceName));
            }
        }
        //@AfterClass
        if(this.afterClass!=null) {
            test.append(this.afterClass.buildMethod(instanceName));
        }
        return test;
    }




    public StringBuilder buildTestTemplate(String classInstanceName){
        StringBuilder template =new StringBuilder();
        String testInstanceName="test";
        template.append("//tests for class:"+getClassName()+"\n");//append comments
        template.append("public class "+classInstanceName+"{\n");

        //build fields
        if(getField().size()!=0){
            for(JUnitField f: getField()){
                template.append("static "+f.buildField()+";\n");
            }
        }

        //build main method
        template.append("public static void main(String[] args){\n");
        // if the test is parameterized Test, then it needs to implement a loop to iterate through each element
        if(isParameterizedTest()){

            template.append(buildLoopStatement(getParameterMethod()));
            if(getTests()!=null) {
                for(int i=0;i<getTests().size();i++) {
                    template.append(buildTest(testInstanceName+i, getTests().get(i)));
                }
            }
            template.append("   }\n");//end of loop
        }else{
            //the constructor has no arguments
            if(getTests()!=null) {
                for(int i=0;i<getTests().size();i++) {
                    template.append("\ttry{\n");
                    template.append(buildTest(testInstanceName+i, getTests().get(i)));
                    template.append("\t}catch(Throwable e){ System.err.println(e.getMessage());}\n");//end of main method
                }
            }
        }
        template.append("}");//end of main method
        template.append("}");//end of class
        return  template;
    }

    private String buildLoopStatement(JUnitMethod parameterMethod){
        String loopStatement="";
        if(getConstructor().getNumberOfArgsInConstructor()==1 || getField().size()==1){//only one argument || one field
            loopStatement="   for(Object this_is_parameter:"+this.getClassName()+"."+parameterMethod.getName()+"()){\n";
        }
        if(getConstructor().getNumberOfArgsInConstructor()>1 || getField().size()>1){// more than 2 arguments || two fields
            loopStatement="   for(Object[] this_is_parameter:"+this.getClassName()+"."+parameterMethod.getName()+"()){\n";
        }
        return loopStatement;
    }

    public void checkForTests(String desc, JUnitMethod test){
            if(desc.equals("Lorg/junit/BeforeClass;")){
                setBeforeClass(test);
            }
            if(desc.equals("Lorg/junit/Before;")){
                setBefore(test);
            }
            if(desc.equals("Lorg/junit/Test;")){
                addTest(test);
            }
            if(desc.equals("Lorg/junit/After;")){
                setAfter(test);
            }
            if(desc.equals("Lorg/junit/AfterClass;")){
                setAfterClass(test);
            }
            if(desc.equals("Lorg/junit/runners/Parameterized$Parameters;")){
                setParameterMethod(test);
            }
    }

}
