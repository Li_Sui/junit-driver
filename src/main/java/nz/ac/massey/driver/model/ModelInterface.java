package nz.ac.massey.driver.model;

import nz.ac.massey.driver.structure.JUnitMethod;
import nz.ac.massey.driver.structure.DriverClass;

import java.util.List;

/**
 * Created by li on 30/05/17.
 */
public interface ModelInterface {

    public StringBuilder buildTestTemplate(String classInstanceName);

    public void checkForTests(String desc, JUnitMethod method);

}
