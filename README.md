This project is for generating Junit test driver


ANT USAGE
------------
*   `ant generate-driver -Dversion=[-junit4|-junit3] -Djar=(jar files with test cases) -Doutput=(output dir)`  ----- generate drivers in build/driver_source_code
*   `ant compile-driver -Dversion=[-junit4|-junit3] -Djar=(jar files with test cases) -Doutput=(output dir)`  ----- run the drivers with respective JUnit version


Note that junit3 is still under developing